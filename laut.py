# coding=utf-8

# this is an ugly hack
from mimetypes import add_type
add_type('image/svg+xml', '.svg')
add_type('text/plain', '.log')

from datetime import datetime
from mpd import MPDClient
from flask import Flask, g, request, session, sessions, render_template, redirect, flash
from flask.ext.sass import Sass
from flask_sockets import Sockets
from json import dumps as json


print "sessions:", sessions

app = Flask('laut')
import config
sockets = Sockets(app)

if not app.debug:
    import logging
    log_handler = logging.FileHandler('static/laut.log', 'a', 'utf-8')
    log_handler.setLevel(logging.INFO)
    app.logger.addHandler(log_handler)
    app.logger.setLevel(logging.INFO)

Sass(app)



class Client(MPDClient):

    def __init__(self, *args, **kwargs):

        kwargs['use_unicode'] = True
        super(Client, self).__init__(*args, **kwargs)

        self.connect(host=app.config['MPD_HOST'], port=app.config['MPD_PORT'])

        if app.config.has_key('MPD_PASSWORD') and app.config['MPD_PASSWORD'] != False:
            self.password(app.config['MPD_PASSWORD'])


def render(template):

    return render_template('boilerplate.jinja', content=template, status=g.status, song=g.song)


@app.before_request
def preprocess():

    g.client = Client()
    g.status = g.client.status()
    g.song = g.client.currentsong()


@app.teardown_request
def postprocess(exception):
    g.client.disconnect()


@app.route('/')
def page_main():

    if int(g.status['volume']) < 0 or g.status['state'] in ['stop', 'pause'] or (app.config.has_key('DISABLED') and app.config['DISABLED'] == True):

        return render('nope.jinja')

    return render('laut.jinja')


@app.route('/', methods=['POST'])
def post_main():

    if request.form.has_key('mode'):

        mode = request.form['mode']

        now = datetime.now()
        app.logger.info('[%.04d-%.02d-%.02d %.02d:%.02d:%.02d] %s requested from %s using %s' % (now.year, now.month, now.day, now.hour, now.minute, now.second, mode, request.remote_addr, request.user_agent))

        volume = int(g.status['volume'])

        if g.status['state'] == 'play':

            if mode == 'piano':

                if volume <= app.config['VOL_MIN']:
                    flash("Dat sollte jetzt doch reichen, wa?", 'error')

                else:

                    new_vol = volume - app.config['VOL_PIANO']
                    if app.config['VOL_MIN'] > new_vol:
                        new_vol = app.config['VOL_MIN']

                    g.client.setvol(new_vol)
                    flash('Piano, Junge!')


            elif mode == 'forte':

                if volume >= app.config['VOL_MAX']:
                    flash("Passt scho.", 'error')

                else:

                    new_vol = volume + app.config['VOL_FORTE']
                    if app.config['VOL_MAX'] < new_vol:
                        new_vol = app.config['VOL_MAX']

                    g.client.setvol(new_vol)
                    flash("Fick ja, lauter muss dem!")


                if g.song.has_key('artist'):
                    flash("Aktuelle Wobs gebaut von %s." % (g.song['artist'],))

        else:
            flash("You did it wrong.", 'error')

    else:
        flash("You did it wrong.", 'error')


    return redirect('/')


@app.route('/song')
def page_song():

    client = Client()
    status = client.status()
    song = client.currentsong()

    if status['state'] == 'play':
        return render('song.jinja')



@sockets.route('/update/volume')
def socket_volume(ws):

    client = Client()
    
    while True:
        client.idle('volume')
        status = client.status()
        
        data = {
            'data-type': 'volume',
            'data-id': 'current',
            'data': {
                'volume': status.get('volume')
            }
        }

        ws.send(json(data))



@sockets.route('/update/song')
def socket_song(ws):

    client = Client()

    while True:
        client.idle('player')
        song = client.currentsong()

        data = {
            'data-type': 'song',
            'data-id': 'current',
            'data': {
                'artist': song.get('artist'),
                'album': song.get('album'),
                'title': song.get('title')
            }
        }

        ws.send(json(data))
