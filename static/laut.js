var _;
window.onload = init;

function init(){

    var volume_socket = new WebSocket('ws://localhost:8000/update/volume');
    var song_socket = new WebSocket('ws://localhost:8000/update/song');

    volume_socket.onmessage = function(event){

        var data = JSON.parse(event.data);
        render(data);
    };


    song_socket.onmessage = function(event){
        
        var data = JSON.parse(event.data);
        render(data);
    };


    // Make sure the volume class is updated, too
    elements = document.querySelectorAll('[data-type="volume"]');
    for(var i = 0; i < elements.length; i++){
        
        var element = elements[i];
        element.addEventListener('DOMSubtreeModified', function(x){
            var volume = this.querySelector('[data-key="volume"]').innerHTML;
            this.className = 'volume vol-'+volume;
        });
        
    }

}

function render(data){


    var elements = document.querySelectorAll('[data-type="'+ data['data-type'] +'"][data-id="'+ data['data-id'] +'"]');

    for(var i = 0; i < elements.length; i++){

        var element = elements[i];

        for(var k in data['data']){

            var subelement = element.querySelector('[data-key="'+ k +'"]');
            var value = data['data'][k];

            if(subelement != null){
                subelement.innerHTML = value;
            }
        }
    }
}
